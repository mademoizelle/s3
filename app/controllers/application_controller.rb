class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def home
  	render template: "pages/home"
  end
end
