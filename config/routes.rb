Rails.application.routes.draw do
	root :to => 'application#home'
  resources :posts
  resources :attachments
  resources :pictures
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
