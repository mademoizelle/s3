json.extract! picture, :id, :imageable_name, :imageable_type, :created_at, :updated_at
json.url picture_url(picture, format: :json)
