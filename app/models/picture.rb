class Picture < ApplicationRecord
  belongs_to :imageable, polymorphic: true, optional: true

  has_attached_file :image, 
  					:styles => { :large => "500x300#", :medium => "300x300>", :thumb => "100x100>" }, 
  					default_url: "/images/:style/missing.png"
  
  validates_attachment :image, :presence => true,
  					   :content_type => { :content_type => /\Aimage\/.*\z/ },
                       :size => { :in => 0..5.megabytes }
end
